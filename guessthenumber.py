from os import system, name
from random import randint
def guessthenumbergame():
    clear()
    print("Welcome to GuessTheNumberGame")
    rand_range = int(input('Enter first number: '))
    rand_range_sec_number = int(input('Enter last number: '))
    rand_numb = randint(rand_range, rand_range_sec_number)
    inp_time_to_retry = int(input("Enter number of attemps: "))
    for i in range(inp_time_to_retry):
        numbergame = int(input("Enter y'r number: "))
        if numbergame > rand_numb:
            print("Number is smaller")
        elif numbergame < rand_numb:
            print("Number is bigger")
        elif numbergame == rand_numb:
            print("Congrats!.Correct answer")
            choisenumbergame = str(input("Do you want to play again (Y or N): "))
            if choisenumbergame == "Y":
                clear()
                guessthenumbergame()
            elif choisenumbergame == "N":
                clear()
                exit()

        else:
            break
    print("You missed.The number was", rand_numb)
def clear():
    if name == 'nt':
        system('cls')
    elif name == 'posix':
        system('clear')
guessthenumbergame()